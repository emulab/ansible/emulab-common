# `emulab.common` Ansible collection

This collection contains common Emulab tasks, modules, and playbooks for
integrating and automating Ansible execution within Emulab/CloudLab/POWDER
experiments.
