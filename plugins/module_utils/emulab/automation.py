from __future__ import absolute_import, print_function

import os
import os.path
from io import open


def convert(v):
    if v in [ "True","true",True ]:
        return True
    elif v in  [ "False","false",False ]:
        return False
    #elif v == None:
    #    return ""
    return v

def file_diff(path,new_content):
    if not os.path.exists(path):
        return True
    content = None
    with open(path,'rb') as f:
        content = f.read().decode("utf-8")
    return content != new_content

def generate_files(module,emulab_facts,manifest_facts,path="/local/setup/ansible"):
    """
    Returns True if generated files will change; respects check_mode.
    """
    ret = False

    if not os.path.exists(path):
        os.makedirs(path)
    inventory_file = os.path.join(path,"inventory.ini")
    overrides_file = os.path.join(path,"vars")
    host_vars_dir = os.path.join(path,"host_vars")
    if not os.path.exists(host_vars_dir):
        os.makedirs(host_vars_dir)
    group_vars_dir = os.path.join(path,"group_vars")
    if not os.path.exists(group_vars_dir):
        os.makedirs(group_vars_dir)
    per_playbook_dir = os.path.join(path,"per_playbook")
    if not os.path.exists(per_playbook_dir):
        os.makedirs(per_playbook_dir)
    entrypoints_dir = os.path.join(path,"entrypoints")
    if not os.path.exists(entrypoints_dir):
        os.makedirs(entrypoints_dir)
    runscript_file = os.path.join(path,"run-automation.sh")

    overrides_content = ""
    for (name,override) in manifest_facts["global_overrides"].items():
        on_empty = False if override.get("on_empty",False) in [ "False","false",False,"0",0,"no","No" ] else True
        if override["value"] == None or override["value"] == "":
            if on_empty:
                overrides_content += "%s: %s%s" % (name,'""',os.linesep)
            else:
                overrides_content += "#%s: %s%s" % (name,'""',os.linesep)
        else:
            overrides_content += "%s: %s%s" % (name,override["value"],os.linesep)
    is_diff = file_diff(overrides_file,overrides_content)
    ret = ret or is_diff
    if is_diff and not module.check_mode:
        with open(overrides_file,"wb") as f:
            f.write(overrides_content.encode("utf-8"))

    inventory_content = ""
    if not "all" in manifest_facts["roles"]:
        inventory_content += "[all]%s" % (os.linesep,)
        for nodename in manifest_facts["nodenames"]:
            #
            # By default, if we have a manifest, only add nodes with roles to
            # the [all] group, unless instructed otherwise.  If we don't have
            # a manifest, we have no roles, so in that case, do add all nodes.
            #
            if emulab_facts["emulab_is_geni"] \
              and not module.params['inventory_allnodes'] \
              and (nodename not in manifest_facts["role_bindings"] \
                   or len(manifest_facts["role_bindings"][nodename]) == 0):
                continue

            #
            # If this is the current node, always use the local connection option.
            # This is necessary in one-node experiments where we do not have a short-name
            # /etc/hosts entry bound to a lan, or if the head node is not connected to any
            # lans.  In the disconnected case, there are more problems we need to solve...
            #
            opts = ""
            if nodename == emulab_facts["emulab_nodename"]:
                opts = "ansible_connection=local"
            inventory_content += "%s %s %s" % (nodename,opts,os.linesep)
        inventory_content += os.linesep
    for (name,role) in manifest_facts["roles"].items():
        inventory_content += "[%s]%s%s%s%s" % (
            role["group"] or name,os.linesep,
            os.linesep.join(role["nodes"]),os.linesep,os.linesep)
    is_diff = file_diff(inventory_file,inventory_content)
    ret = ret or is_diff
    if is_diff and not module.check_mode:
        with open(inventory_file,"wb") as f:
            f.write(inventory_content.encode("utf-8"))

    runscript_lines = [
        "#!/bin/sh",
        "",
        "#",
        "# This file is AUTOGENENERATED by ansible.module_utils.emulab.automation; DO NOT MODIFY!",
        "# It runs the automatic entrypoints in %s ." % (entrypoints_dir,),
        "#",
        "",
        "set -e",
        "",
        "cd %s" % (path,),
        ""
    ]
    for name in manifest_facts["role_order"]:
        role = manifest_facts["roles"].get(name,None)
        if not role:
            continue
        if not role.get("playbook_order",None):
            continue

        entrypoint_file = os.path.join(entrypoints_dir,name)
        entrypoint_lines = [
            "#!/bin/sh",
            "",
            "#",
            "# This file is AUTOGENENERATED by ansible.module_utils.emulab.automation; DO NOT MODIFY!",
            "# It runs the '%s' role." % (name,),
            "#",
            "",
            "set -e",
            "",
            "cd %s" % (path,),
            "",
            "[ -e /local/setup/venv/default/bin/activate ] && . /local/setup/venv/default/bin/activate",
            ""
        ]

        source = role.get("source")
        source_type = role.get("source_type")
        if source:
            if source_type == "galaxy":
                # In this case, the role was installed into the standard path,
                # so nowhere to cd.
                entrypoint_lines.extend([
                    'ansible-galaxy role install "%s" \\' % (source,),
                    '    || (echo "ERROR: failed to install role (%s) via galaxy" ; exit 1)' % (source,)
                ])
            else:
                # In this case, change to the fetched dir
                fetchpath = os.path.join(path,role.get("path",name))
                entrypoint_lines.extend([
                    'git clone "%s" "%s" \\' % (source,fetchpath),
                    '    || (echo "ERROR: failed to clone role (%s) via git" ; exit 1)' % (source,),
                    'cd %s' % (fetchpath,),
                ])
        else:
            # In this case, the role is in the source tree, and this is a
            # relative path into that.
            entrypoint_lines.extend([
                'cd /local/repository/%s' % (role.get("path",name)),
            ])
        for pname in role.get("playbook_order",[]):
            playbook = role["playbooks"][pname]
            if "pre_hook" in playbook:
                entrypoint_lines.extend([playbook["pre_hook"]])
            become_opts = ""
            if playbook.get("become",None):
                become_opts = "-b --become-user=" + playbook["become"]
            entrypoint_lines.extend([
                "OVERRIDES_FILE=%s" % (overrides_file,),
                "OVERRIDES_FILE_ARGS=",
                "if [ -s $OVERRIDES_FILE ]; then",
                '    OVERRIDES_FILE_ARGS="-e @$OVERRIDES_FILE"',
                "fi",
                "ansible-playbook %s -v -i %s $OVERRIDES_FILE_ARGS %s $EXTRA_OVERRIDES" % (
                    become_opts,inventory_file,playbook.get("path",pname)),
            ])
            if "post_hook" in playbook:
                entrypoint_lines.extend([playbook["post_hook"]])

        entrypoint_content = os.linesep.join(entrypoint_lines)
        is_diff = file_diff(entrypoint_file,entrypoint_content)
        ret = ret or is_diff
        if is_diff and not module.check_mode:
            with open(entrypoint_file,"wb") as f:
                f.write(entrypoint_content.encode("utf-8"))
            os.chmod(entrypoint_file,0o755)

        if convert(role.get("auto",True)):
            runscript_lines.extend([entrypoint_file,""])

    runscript_content = os.linesep.join(runscript_lines)
    is_diff = file_diff(runscript_file,runscript_content)
    ret = ret or is_diff
    if is_diff and not module.check_mode:
        with open(runscript_file,"wb") as f:
            f.write(runscript_content.encode("utf-8"))
        os.chmod(runscript_file,0o755)

    return ret
