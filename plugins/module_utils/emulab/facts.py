from __future__ import absolute_import, print_function

import os
import os.path
from io import open
import re

BOOTDIR = "/var/emulab/boot"
TOPOMAP = BOOTDIR + "/topomap"
TOPOMAP_COMMENT_REGEX = re.compile("^[ \t]*#")
TOPOMAP_LAN_REGEX = re.compile("^([^,]+),([^,]+),([^,]+)$")
TOPOMAP_NODE_REGEX = re.compile("^([^,]+),(.*)$")
TMCC = "/usr/local/etc/emulab/tmcc.bin"

file_fact_map = {
    BOOTDIR + "/bossip": "emulab_bossip",
    BOOTDIR + "/controlif": "emulab_controlif",
    BOOTDIR + "/creator": "emulab_creator_legacy",
    BOOTDIR + "/eventkey": "emulab_eventkey",
    BOOTDIR + "/keyhash": "emulab_keyhash",
    BOOTDIR + "/localevserver": "emulab_localevserver",
    BOOTDIR + "/mydomain": "emulab_control_domain",
    BOOTDIR + "/myip": "emulab_myip",
    BOOTDIR + "/mynetmask": "emulab_control_netmask",
    BOOTDIR + "/nickname": "emulab_nickname",
    BOOTDIR + "/nodeid": "emulab_nodeid",
    BOOTDIR + "/nodetype": "emulab_nodetype",
    BOOTDIR + "/nodeuuid": "emulab_nodeuuid",
    BOOTDIR + "/realname": "emulab_realname",
    BOOTDIR + "/role": "emulab_role",
    BOOTDIR + "/routerip": "emulab_control_router_ip",
    BOOTDIR + "/swapper": "emulab_swapper_legacy",
    BOOTDIR + "/syncserver": "emulab_syncserver",
    BOOTDIR + "/vmname": "emulab_vmname",
}

tmcc_fact_map = dict(
    geni_client_id="geni_client_id",
    geni_slice_urn="geni_slice_urn",
    geni_slice_email="geni_slice_email",
    geni_user_urn="geni_user_urn",
    geni_user_email="geni_user_email",
    geni_geniuser="geni_geniuser",
    geni_manifest="geni_manifest_content",
    geni_key="geni_key",
    geni_certificate="geni_certificate",
    geni_control_mac="geni_control_mac",
    geni_version="geni_version",
    geni_sliverstatus="geni_sliverstatus",
    geni_status="geni_status",
    geni_rpccert="geni_rpccert",
    geni_portalmanifest="geni_portal_manifest_content",
)

def gather_topomap(module):
    topomap_facts_dict = dict(nodes=dict(),lans=dict(),node_list=[],lan_list=[])

    if not os.path.exists(TOPOMAP):
        return {}

    f = open(TOPOMAP,"rt")
    for line in f:
        if TOPOMAP_COMMENT_REGEX.match(line):
            continue

        m = TOPOMAP_LAN_REGEX.match(line)
        if m:
            name = m.group(1)
            topomap_facts_dict["lan_list"].append(name)
            ldict = dict(name=name,mask=m.group(2),cost=m.group(3))
            topomap_facts_dict["lans"][name] = ldict
            continue

        m = TOPOMAP_NODE_REGEX.match(line)
        if m:
            name = m.group(1)
            lans = m.group(2).strip().split(" ")
            topomap_facts_dict["node_list"].append(name)
            ndict = dict(name=name,lans={})
            for lan in lans:
                if not lan:
                    continue
                [lanname,lanip] = lan.split(":")
                ndict["lans"][lanname] = lanip
            topomap_facts_dict["nodes"][name] = ndict
    f.close()

    return topomap_facts_dict

def gather_facts(module):
    facts_dict = dict()

    if os.path.exists(BOOTDIR):
        facts_dict["emulab"] = True
    else:
        facts_dict["emulab"] = False
        return facts_dict

    for (filename,factname) in file_fact_map.items():
        if os.path.exists(filename):
            f = open(filename,"rb")
            text = f.read().decode("utf-8")
            f.close()
            if text:
                text = text.lstrip("\0").rstrip("\n")
                facts_dict[factname] = text

    for (tcommand,factname) in tmcc_fact_map.items():
        (rc,stdout,stderr) = module.run_command([TMCC,tcommand])
        if rc:
            continue
        if stdout:
            stdout = stdout.lstrip("\0").rstrip("\n")
            facts_dict[factname] = stdout

    geni_user_urn = facts_dict.get("geni_user_urn",None)
    if geni_user_urn:
        facts_dict["emulab_is_geni"] = is_geni = True
        facts_dict["geni_user"] = geni_user_urn.split("+")[-1]
    else:
        facts_dict["emulab_is_geni_expt"] = is_geni = False

    if is_geni:
        facts_dict["emulab_swapper"] = facts_dict["geni_user"]
        facts_dict["emulab_creator"] = facts_dict["geni_user"]
        facts_dict["emulab_swapper_email"] = facts_dict["geni_user_email"]
        facts_dict["emulab_creator_email"] = facts_dict["geni_user_email"]
    else:
        facts_dict["emulab_swapper"] = facts_dict["emulab_swapper_legacy"]
        facts_dict["emulab_creator"] = facts_dict["emulab_creator_legacy"]
        facts_dict["emulab_swapper_email"] = \
            facts_dict["emulab_swapper"] + "@" + facts_dict["emulab_control_domain"]
        facts_dict["emulab_creator_email"] = facts_dict["emulab_swapper_email"]

    emulab_myip = facts_dict.get("emulab_myip")
    if emulab_myip:
        facts_dict["emulab_control_ip"] = emulab_myip

    emulab_nodeid = facts_dict.get("emulab_nodeid",None)
    emulab_nickname = facts_dict.get("emulab_nickname",None)
    emulab_control_domain = facts_dict.get("emulab_control_domain",None)
    if emulab_nodeid and emulab_control_domain:
        facts_dict["emulab_control_fqdn"] = emulab_nodeid + "." + emulab_control_domain
    if emulab_nickname and emulab_control_domain:
        facts_dict["emulab_fqdn"] = emulab_nickname + "." + emulab_control_domain
    if emulab_nickname:
        ena = emulab_nickname.split(".")
        facts_dict["emulab_nodename"] = ena[0]
        facts_dict["emulab_eid"] = ena[1]
        facts_dict["emulab_pid"] = ena[2]
        if emulab_control_domain:
            facts_dict["emulab_domain"] = ena[1] + "." + ena[2] + "." + emulab_control_domain
    if "emulab_pid" in facts_dict:
        facts_dict["emulab_swapper_group"] = facts_dict["emulab_pid"]

    facts_dict["emulab_topomap"] = gather_topomap(module)
    if "nodes" in facts_dict["emulab_topomap"]:
        facts_dict["emulab_all_nodenames"] = list(facts_dict["emulab_topomap"]["nodes"])
    else:
        facts_dict["emulab_all_nodenames"] = []
    if emulab_nickname and facts_dict["emulab_topomap"].get('nodes',{}).get(facts_dict["emulab_nodename"]).get('lans',{}) != {}:
        facts_dict["emulab_node_has_lans"] = True
    else:
        facts_dict["emulab_node_has_lans"] = False
    if facts_dict["emulab_topomap"].get("lan_list",[]):
        facts_dict["emulab_expt_has_lans"] = True
    else:
        facts_dict["emulab_expt_has_lans"] = False

    facts_dict["emulab_swapper"] = \
        facts_dict.get("geni_user",facts_dict.get("emulab_swapper"))
    facts_dict["emulab_real_swapper"] = \
        facts_dict.get("geni_user",facts_dict.get("emulab_swapper"))

    return facts_dict
