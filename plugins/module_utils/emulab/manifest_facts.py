
from __future__ import absolute_import, print_function

import os
import os.path
from io import open
import lxml.etree

EMULAB_EXTNS = "http://www.protogeni.net/resources/rspec/ext/emulab/1"
WORKFLOW_EXTNS = "http://www.protogeni.net/resources/rspec/ext/workflow/1"
TMCC = "/usr/local/etc/emulab/tmcc.bin"

geni_key = None
manifest = None

#
# XXX: cryptography lacks support for smime decrypt --
# https://github.com/pyca/cryptography/issues/6263 .
# At least we don't have to write the ciphertext to disk,
# not that it matters since all this data is available via
# tmcd.
#
def decrypt(module,ciphertext):
    global geni_key
    if not geni_key:
        (rc,stdout,stderr) = module.run_command([TMCC,"geni_key"])
        geni_key = stdout.lstrip("\0").rstrip("\n")
    geni_key_path = os.path.join(module.tmpdir,"geni.key")
    with open(geni_key_path,'wb') as f:
        f.write(geni_key.encode("utf-8"))
    command = [ "/usr/bin/openssl", "smime", "-decrypt", "-inform", "PEM",
                "-inkey", geni_key_path, "-in", "-" ]
    (rc,stdout,stderr) = module.run_command(command,data=ciphertext)#,binary_data=True)
    os.unlink(geni_key_path)
    return stdout

def convert(p,v):
    if v in [ "True","true",True ]:
        return True
    elif v in  [ "False","false",False ]:
        return False
    #elif v == None:
    #    return ""
    return v

def gather_facts(module):
    global manifest

    if not manifest:
        (rc,stdout,stderr) = module.run_command([TMCC,"geni_manifest"])
        if rc or not stdout:
            return dict()
        manifest = stdout.lstrip("\0")

    root = lxml.etree.fromstring(manifest)

    facts_dict = dict()

    # Find nodes
    nodes = dict()
    for elm in root.getchildren():
        if not elm.tag.endswith("}node"):
            continue
        (host,ip) = (None,None)
        for elm2 in elm.getchildren():
            if elm2.tag.endswith("}host"):
                (host,ip) = (elm2.get("name",None),elm2.get("ipv4",None))
                break
        nodes[elm.get("client_id")] = dict(hostname=host,ip=ip)
    facts_dict["nodes"] = nodes
    facts_dict["nodenames"] = list(nodes)

    # Find parameters
    parameters = dict()
    for elm in root.getchildren():
        if elm.tag.endswith("}data_set"):
            for elm2 in elm.getchildren():
                if elm2.tag.endswith("}data_item"):
                    p = elm2.get("name")
                    v = convert(p,elm2.text)
                    parameters[p] = v
        if elm.tag.endswith("}data_item"):
            p = elm.get("name")
            parameters[p] = convert(p,elm.text)
    facts_dict["parameters"] = parameters

    # Find all the public IP addresses
    routable_pools = dict()
    for elm in root.getchildren():
        if not elm.tag.endswith("}routable_pool"):
            continue
        pool_name = elm.get("client_id")
        if not pool_name in routable_pools:
            routable_pools[pool_name] = []
        for elm2 in elm.getchildren():
            if elm2.tag.endswith("}ipv4"):
                routable_pools[pool_name].append(
                    [elm2.get("address"),elm2.get("netmask")])
    facts_dict["routable_pools"] = routable_pools

    # Find any passwords for decrypt
    passwords = dict()
    for elm in root.getchildren():
        if not elm.tag.endswith("}password"):
            continue
        passwords[elm.get("name")] = decrypt(module,elm.text)
    facts_dict["passwords"] = passwords

    # Dig through ansible extensions and maybe generate some inventories,
    # overrides, and playbook wrappers.  First we grab the roles/collections/playbooks,
    # then the bindings, then the overrides, then the script.
    roles = dict()
    role_order = []
    for elm in root.getchildren():
        if not elm.tag.endswith("}role"):
            continue
        playbook_order = []
        playbooks = dict()
        role_order.append(elm.get("name"))
        roles[elm.get("name")] = dict(
            path=elm.get("path"),source=elm.get("source",""),
            group=elm.get("group",""),auto=elm.get("auto",True),
            playbooks=playbooks,playbook_order=playbook_order,nodes=[])
        for elm2 in elm.getchildren():
            if not elm2.tag.endswith("}playbook"):
                continue
            pname = elm2.get("name")
            playbook_order.append(pname)
            playbooks[pname] = dict(
                path=elm2.get("path"),inventory_name=elm2.get("inventory_name"),
                overrides_name=elm2.get("overrides_name"),
                inventory_generator_path=elm2.get("inventory_generator_path"),
                overrides_generator_path=elm2.get("overrides_generator_path"),
                runner_path=elm2.get("runner_path"),pre_hook=elm2.get("pre_hook"),
                post_hook=elm2.get("post_hook"),become=elm2.get("become"))
    facts_dict["role_order"] = role_order
    facts_dict["roles"] = roles

    role_bindings = dict()
    for elm in root.getchildren():
        bound_roles = []
        if not elm.tag.endswith("}node"):
            continue
        nodename = elm.get("client_id")
        for elm2 in elm.getchildren():
            if not elm2.tag.endswith("}role_binding"):
                continue
            role = elm2.get("role")
            bound_roles.append(role)
            if role in roles:
                roles[role]["nodes"].append(nodename)
        if bound_roles:
            role_bindings[elm.get("client_id")] = bound_roles
    facts_dict["role_bindings"] = role_bindings

    global_overrides = dict()
    pernode_overrides = dict()
    for elm in root.getchildren():
        if elm.tag.endswith("}override"):
            source = elm.get("source")
            lsource = source + "s"
            source_name = elm.get("source_name")
            value = None
            if lsource in facts_dict:
                value = facts_dict[lsource].get(source_name,None)
            elif not source:
                value = elm.text
            global_overrides[elm.get("name")] = dict(
                source=lsource,source_name=source_name,value=value)
        if elm.tag.endswith("}node"):
            nodename = elm.get("client_id")
            for elm2 in root.getchildren():
                if elm2.tag.endswith("}override"):
                    name2 = elm2.get("name")
                    source2 = elm2.get("source")
                    lsource2 = source2 + "s"
                    source_name2 = elm2.get("source_name")
                    value2 = None
                    if lsource2 in facts_dict:
                        value2 = facts_dict[lsource2].get(source_name2,None)
                    elif not source2:
                        value2 = elm2.text
                    if not nodename in pernode_overrides:
                        pernode_overrides[nodename] = dict()
                    pernode_overrides[nodename][name2] = dict(
                        source=lsource2,source_name=source_name2,value=value2)
    facts_dict["global_overrides"] = global_overrides
    facts_dict["pernode_overrides"] = pernode_overrides

    return facts_dict
