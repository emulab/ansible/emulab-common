
from __future__ import absolute_import, print_function
__metaclass__ = type

DOCUMENTATION = '''
module: gather_manifest_facts
short_description: Gather Emulab experiment manifest facts
description:
  - Gather Emulab experiment manifest facts
requirements:
  - Requires python-lxml for Emulab manifest processing.
author:
  - David Johnson (@carboxylman)
attributes:
  check_mode:
    support: full
  diff_mode:
    support: none
  facts:
    support: full
  platform:
    platforms: posix
'''

EXAMPLES = '''
- name: Gather Emulab experiment manifest facts
  gather_manifest_facts: ""
'''

RETURN = '''
ansible_facts:
  description: Emulab experiment manifest facts to add to ansible_facts.
  returned: always
  type: complex
'''

from ansible.module_utils.basic import AnsibleModule

import ansible_collections.emulab.common.plugins.module_utils.emulab.manifest_facts as emulab_manifest_facts

def main():
    module = AnsibleModule(
        argument_spec=dict(),
        supports_check_mode=True
    )

    geni_manifest_facts = emulab_manifest_facts.gather_facts(module)

    module.exit_json(
        changed=False,
        ansible_facts=dict(geni_manifest=geni_manifest_facts))

if __name__ == "__main__":
    main()
