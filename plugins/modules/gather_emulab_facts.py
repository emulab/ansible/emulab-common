
from __future__ import absolute_import, print_function
__metaclass__ = type

DOCUMENTATION = '''
module: emulab_facts
short_description: Emulab experiment-wide and per-node fact gathering
description:
  - Return information about Emulab experiments and nodes as facts.
requirements:
  - Requires python-lxml for Emulab manifest processing.
author:
  - David Johnson (@carboxylman)
attributes:
  check_mode:
    support: full
  diff_mode:
    support: none
  facts:
    support: full
  platform:
    platforms: posix
'''

EXAMPLES = '''
- name: Gather Emulab facts
  gather_emulab_facts: ""
'''

RETURN = '''
ansible_facts:
  description: Emulab facts to add to ansible_facts.
  returned: always
  type: complex
'''

from ansible.module_utils.basic import AnsibleModule

import ansible_collections.emulab.common.plugins.module_utils.emulab.facts as emulab_facts

def main():
    module = AnsibleModule(
        argument_spec=dict(),
        supports_check_mode=True
    )

    if module.check_mode:
        module.exit_json(**result)

    ansible_facts = emulab_facts.gather_facts(module)

    module.exit_json(changed=False,ansible_facts=ansible_facts)

if __name__ == "__main__":
    main()
