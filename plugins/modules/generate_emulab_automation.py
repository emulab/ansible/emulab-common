
from __future__ import absolute_import, division, print_function
__metaclass__ = type

DOCUMENTATION = '''
module: generate_emulab_automation
short_description: Generate Emulab Ansible automation scripts
description:
  - Generate Emulab Ansible automation scripts
author:
  - David Johnson (@carboxylman)
attributes:
  check_mode:
    support: full
  diff_mode:
    support: none
  facts:
    support: full
  platform:
    platforms: posix
options:
  inventory_allnodes:
    description: Set true to force all nodes in an experiment into the inventory [all] group.  If unset, if not a GENI experiment, all nodes will also be added to the [all] group.  Otherwise, only nodes with bound roles will be added to the [all] group.
    required: false
    type: bool
    default: false
'''

EXAMPLES = '''
- name: Generate Emulab Ansible automation scripts
  generate_emulab_automation: ""
'''

RETURN = '''
'''

import os
import os.path
from io import open
import re
import lxml.etree

from ansible.module_utils.basic import AnsibleModule

import ansible_collections.emulab.common.plugins.module_utils.emulab.facts as emulab_facts
import ansible_collections.emulab.common.plugins.module_utils.emulab.manifest_facts as emulab_manifest_facts
import ansible_collections.emulab.common.plugins.module_utils.emulab.automation as emulab_automation

def main():
    module = AnsibleModule(
        argument_spec=dict(
            inventory_allnodes=dict(type="bool", required=False, default=False)
        ),
        supports_check_mode=True
    )
    #if module.check_mode:
    #    module.exit_json(**result)

    facts = emulab_facts.gather_facts(module)
    manifest_facts = emulab_manifest_facts.gather_facts(module)
    changed = emulab_automation.generate_files(module,facts,manifest_facts)

    module.exit_json(changed=changed)

if __name__ == "__main__":
    main()
